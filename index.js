class MyArray {
  constructor() {
    this.length = 0
    for (let i = 0; i < arguments.length; i++) {
      this[i] = arguments[i]
      this.length += 1
    }
  }

  push(value) {
    this[this.length] = value
    return ++this.length
  }

  pop() {
    const lastEl = this[this.length - 1]

    delete this[this.length - 1]
    this.length -= 1

    return lastEl
  }

  map(cb) {
    let newArray = new MyArray()

    for (let i = 0; i < this.length; i++) {
      newArray.push(cb(this[i], i, this))
    }

    return newArray
  }

  forEach(cb) {
    for (let i = 0; i < this.length; i++) {
      cb(this[i], i, this)
    }
  }

  reduce(cb, initialValue = this[0]) {
    for (let i = 0; i < this.length; i++) {
      initialValue = cb(initialValue, this[i], i, this)
    }

    return initialValue
  }

  filter(cb) {
    let newArray = new MyArray()

    for (let i = 0; i < this.length; i++) {
      if (cb(this[i], i, this)) {
        newArray.push(this[i])
      }
    }

    return newArray
  }

  sort(cb) {
    for (let i = 0; i < this.length; i++) {
      for (let j = 0; j < this.length; j++) {
        if (cb(this[j], this[j + 1]) > 0) {
          [this[j], this[j + 1]] = [this[j + 1], this[j]];
        }
      }
    }

    return this
  }

  toString() {
    let str = ''

    for (let i = 0; i < this.length; i++) {
      str += `,${this[i]}`
    }

    return str.slice(1)
  }

  static from(item) {
    const newArray = new MyArray()

    for (let i = 0; i < item.length; i++) {
      newArray.push(item[i])
    }

    return newArray
  }

  [Symbol.iterator]() {
    const last = this.length
    let current = 0

    return {
      next: () => {
        if (current < last) {
          return {done: false, value: this[current++]}
        }

        return {done: true}
      },
    }
  }
}

const arr = new MyArray(5, 4, 2, 2, 3, 5, 4, 1)
